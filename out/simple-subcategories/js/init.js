(function(exports) {
    'use strict';
    //initialize the app
    var settings = {
        Model: JSONMediaModel,
        PlayerView: PlayerView,
        PlaylistView: PlaylistPlayerView,
        // dataURL: "https://videos.warwickschiller.com/wp-json/warwickapi/v1/homedetail?user_id=7921",
        dataURL: "./assets/genericSubCategoriesData.json",

        showSearch: true,
        displayButtons:true
    };

    exports.app = new App(settings);
}(window));
