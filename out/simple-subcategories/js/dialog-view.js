/* Dialog View
 *
 * Handles modal dialog
 *
 */

(function (exports) {
    "use strict";

    /**
     * @class DialogView
     * @description The generic dialog view for modal dialogs
     */
    function DialogView(dialogData) {
        Events.call(this, ['exit', 'back', 'loginSuccess', 'logoutSuccess']);
        // console.log(dialogData);
        // debugger;
        this.title = dialogData.title;
        this.message = dialogData.message;
        // this.inputEmail = 'Email';
        this.buttons = dialogData.buttons;
        this.buttonView = null;
        this.upSideFlag = 0;
        this.downSideFlag = 0;
        this.loginInputView = null;
        this.$el = null;

        /**
         * renders the dialog to show the user, can have up to two buttons with callbacks
         * @param {object} $container element that contains the dialog
         */
        this.render = function ($container) {
            var html = utils.buildTemplate($("#dialog-template"), dialogData);
            $container.append(html);
            this.$el = $container.children().last();

            // set up the button view
            var buttonView = this.buttonView = new ButtonView();
            if (!dialogData.isLogin){
                var loginInputView = this.loginInputView = new LogInputView();
                loginInputView.render(this.$el.find(".dialog-buttons-container"));
                // this.loginInputView.selectLeftNavItem(0);
            }
            // console.log(loginInputView);
            // debugger;
            //Create a buttons array for the buttons you want to add
            var buttonArr = [];
            for (var i = 0; i < this.buttons.length; i++) {
                buttonArr.push({
                    id: this.buttons[i].id,
                    buttonValue: this.buttons[i].text
                });
            }


        //    this.loginInputView.updateCurrentSelectedIndex(0);
            // debugger;

            // render the button view and select the first button
            buttonView.render(this.$el.find(".dialog-buttons-container"), buttonArr, this.handleButtonCallback);
            this.buttonView.updateCurrentSelectedIndex(0);

            // this.buttonView.updateCurrentSelectedIndex(0);


            // reselect the same button on the exit event, basically throw away the exit event in this dialog
            buttonView.on("exit", function() {
                // var loginInputView = new LogInputView();
                // debugger
                // this.loginInputView.select();
                // console.log(loginInputView);
                // this.buttonView.updateCurrentSelectedIndex(this.buttonView.selectedButtonIndex);
            }.bind(this));
        };

        /**
         * handles the button click callback and passes it to the correct dialogData button callback
         * @param {object} the button which was pressed by the user
         */
        this.handleButtonCallback = function(button) {
            var clickedButton = $(button).attr('id');
            // find the matching button from the buttons list to call the callback
            for (var i = 0; i < this.buttons.length; i++) {
                if (this.buttons[i].id === clickedButton) {

                    if (clickedButton == 'ok') {
                        this.trigger('exit');
                    }

                    if (clickedButton == 'login') {

                        this.loginInputView.checkCredentials();
                        if (this.loginInputView.loggedUserId){
                            this.trigger('loginSuccess', this.loginInputView.loggedUserName);
                        }
                    }

                    if (clickedButton == 'logout') {
                         this.trigger('logoutSuccess');
                    }
                    // this.buttons[i].callback(this.buttons[i]);
                    return;
                }
            }
        }.bind(this);

        /**
         * Handle key events
         * @param {event} the keydown event
         */
        this.handleControls = function (e) {
            var dirty = false;
            // pressing play triggers select on the media element
            if (e.type === 'buttonpress') {
                switch (e.keyCode) {
                    case buttons.UP:
                        console.log('UP');
                        if (this.upSideFlag == 0){
                            this.buttonView.updateCurrentSelectedIndex(-1);
                            this.loginInputView.incrementCurrentSelectedIndex(-1);
                            this.loginInputView.selectLeftNavItem();
                            this.upSideFlag = 1;
                        }else{

                        }
                        // selectLeftNavItem


                        // this.incrementCurrentSelectedIndex(-1);
                        // debugger;
                        //  this.setChosenElement();
                        // this.trigger('up');
                        //  debugger;
                        // if (this.buttonView) { //content buttons are visible
                        //     if (this.currentView !== this.buttonView) {
                        //         this.transitionToButtonView();
                        //     } else {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // } else { //no buttons
                        //     if (this.currentView === this.shovelerView) {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // }

                        // if (this.currentView !== this.shovelerView) {
                        //     this.transitionToShovelerView();
                        // } else {
                        //     this.trigger('bounce', null);
                        // }

                        dirty = true;
                        break;
                    case buttons.DOWN:
                        console.log('DOWN');

                        this.loginInputView.incrementCurrentSelectedIndex(1);
                        this.loginInputView.selectLeftNavItem();
                        // debugger;
                        // this.trigger('deselect');
                        // this.incrementCurrentSelectedIndex(-1);
                        // this.trigger('down');
                        //handle button view if we have one
                        // if (this.buttonView) { //content buttons are visible
                        //     if (this.currentView !== this.buttonView) {
                        //         this.transitionToButtonView();
                        //     } else {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // } else { //no buttons
                        //     if (this.currentView === this.shovelerView) {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // }

                        dirty = true;
                        break;
                    case buttons.LEFT:
                    console.log('LEFT');
                    // debugger;
                        this.buttonView.handleControls(e);
                        dirty = true;
                        break;
                    case buttons.RIGHT:
                        console.log('RIGHT');
                    // debugger;
                        this.buttonView.handleControls(e);
                        dirty = true;
                        break;
                    case buttons.SELECT:
                        this.buttonView.handleControls(e);
                        dirty = true;
                        break;
                }
            }
            //
            // this.loginInputView.handleControls(e);

        }.bind(this);

        /**
         * Remove the dialog from the app
         */
        this.remove = function() {
            this.$el.remove();
        };

        this.back = function () {
            // debugger;
        };
    }

    exports.DialogView = DialogView;
}(window));
