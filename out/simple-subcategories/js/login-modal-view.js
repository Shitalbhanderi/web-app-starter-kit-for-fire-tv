(function (exports) {
    "use strict";


    /**
     * @class SearchInputView
     * @description The search row in the left nav view.
     */
    function LoginModalView() {
        // mixin inheritance, initialize this as an event handler for these events:
        Events.call(this, ['exit', 'deselect', 'indexChange', 'select', 'makeActive', 'loadComplete', 'bounce', 'up', 'down']);
        // this.currentSearchQuery = null;
        this.$el = null;
        this.currentView = null;

        this.render = function ($el, displayButtonsParam,isLogIn) {
            var loginInputView = this.loginInputView = new LogInputView();
            if (!isLogIn){
                var html = utils.buildTemplate($("#login-modal-template"), {});
                $el.append(html);
                //  this.$el = $parent.children().eq(0);
                 this.$el = $el.children().last();
                 this.el = this.$el[0];
                //  debugger
                this.createButtonView(displayButtonsParam);
                // this.setCurrentView(this.$el);
            }else{
                var html = utils.buildTemplate($("#login-modal-template"), {});
                $el.append(html);
                //  this.$el = $parent.children().eq(0);
                this.$el = $el.children().last();
                this.el = this.$el[0];

                this.createButtonView(displayButtonsParam, isLogIn);
                // this.setCurrentView(this.$el);
            }
        };

        this.createButtonView = function (displayButtonsParam,userName = "Login") {
            // debugger;
            var parentView = this;

            if (!displayButtonsParam) {
                return;
            }

            // create and set up the 1D view
            var buttonView = this.buttonView = new ButtonView();

            buttonView.on('exit', function () {
                this.trigger('exit');
            }, this);

            buttonView.showAsSelected = function () {
                parentView.transitionToButtonView();
            };

            buttonView.handleButtonCallback = function () {
                //add button functionality here
                // debugger;
                console.log(arguments);
            };

            //Create a buttons array for the buttons you want to add
            var buttonArr = [{
                    "id": "buttonLogin",
                    "buttonValue": userName
                },
            ];

            console.log(this.$el);
            // debugger;
            buttonView.render(this.$el.find("#login"), buttonArr, buttonView.handleButtonCallback);

        };

        /**
         * Make the buttons the active view
         */
        this.transitionToButtonView = function () {
            //change to button view
            // debugger;
            this.setCurrentView(this.buttonView);

            //change opacity of the shoveler
            // this.shovelerView.fadeSelected();

            //set default selected button and apply selected style
            this.buttonView.setCurrentSelectedIndex(0);
            this.buttonView.setSelectedButton();
        };
        /**
         * Handle key events
         * @param {event} the keydown event
         */
        this.handleControls = function (e) {
            var dirty = false;
            // pressing play triggers select on the media element
            if (e.type === 'buttonpress') {
                switch (e.keyCode) {
                    case buttons.UP:
                    //  this.setChosenElement();
                     this.trigger('up');
                     //  debugger;
                        // if (this.buttonView) { //content buttons are visible
                        //     if (this.currentView !== this.buttonView) {
                        //         this.transitionToButtonView();
                        //     } else {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // } else { //no buttons
                        //     if (this.currentView === this.shovelerView) {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // }

                        // if (this.currentView !== this.shovelerView) {
                        //     this.transitionToShovelerView();
                        // } else {
                        //     this.trigger('bounce', null);
                        // }

                        dirty = true;
                        break;
                    case buttons.DOWN:
                        // this.trigger('deselect');
                        // this.incrementCurrentSelectedIndex(-1);
                        this.trigger('down');
                        //handle button view if we have one
                        // if (this.buttonView) { //content buttons are visible
                        //     if (this.currentView !== this.buttonView) {
                        //         this.transitionToButtonView();
                        //     } else {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // } else { //no buttons
                        //     if (this.currentView === this.shovelerView) {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // }

                        dirty = true;
                        break;
                    case buttons.SELECT:
                        this.trigger('select');
                        // debugger;
                        break;
                }
            }

        }.bind(this);

        /**
         * Maintain the current view for event handling
         */
        this.setCurrentView = function (view) {
            this.currentView = view;
        };

         /**
          * Increment the index of the currently selected item
          * relative to the last selected index.
          * @param {number} direction to move the left nav
          */
         this.incrementCurrentSelectedIndex = function (direction) {

             if ((direction > 0 && this.currSelectedIndex !== (this.$menuItems.length - 1)) || (direction < 0 && this.currSelectedIndex !== 0)) {
                 this.currSelectedIndex += direction;
                //  this.selectLeftNavItem();
             }
         };
        /**
         * Remove the oneDView element
         */
        this.remove = function () {
            if (this.el) {
                $(this.el).remove();
            }
        };

         this.select = function () {
             this.$el.focus();
             debugger;
         }.bind(this);

         this.exit = function () {
            //  this.$el.focus();
             debugger;
         }.bind(this);

         this.reset = function () {
             this.currentSearchQuery = null;
             this.$el.val("");
         }.bind(this);

         this.deselect = function () {
             this.$el.blur();
         }.bind(this);

    }

    exports.LoginModalView = LoginModalView;
}(window));