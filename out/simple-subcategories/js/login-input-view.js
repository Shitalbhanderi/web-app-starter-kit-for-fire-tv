/* Search Input View
 *
 * Search Widget for a row view container like the left nav view.
 *
 */
(function (exports) {
    "use strict";

    //gloabl constants
    var CONTAINER_SCROLLING_LIST = "#login-input-list",

        CONTAINER_MAIN = "#login-input-container",

        CLASS_MENU_ITEM_SELECTED = "leftnav-list-item-selected",

        CLASS_MENU_ITEM_HIGHLIGHTED = "leftnav-list-item-highlighted",

        CLASS_MENU_ITEM_CHOSEN = "leftnav-list-item-chosen";

    /**
     * @class LogInputView
     * @description The search row in the left nav view.
     */
    function LogInputView() {
        // mixin inheritance, initialize this as an event handler for these events:
        Events.call(this, ['checkCredentials', 'loginSuccess', 'indexChange']);
        this.currentSearchQuery = null;
        this.isLoginUser = null;
        this.loggedUserName = null;
        this.loggedUserId = null;
        this.leftNavContainerEle = null;
        this.$el = null;
        this.currSelectedIndex = 0;
        this.$menuItems = [];
        this.currentSelectionEle = null;
        this.scrollingContainerEle = null;

        this.render = function ($parent) {
            var html = utils.buildTemplate($("#login-input-template"), {});
            $parent.html(html);

            this.$el = $parent.children().children().children().eq(0);
            // console.log(this.$el);
            // debugger;
            this.$menuItems = $(CONTAINER_SCROLLING_LIST).children();
            this.currSelectedIndex = 0;
            this.currentSelectionEle = this.$menuItems.eq(this.currSelectedIndex)[0];
            // console.log(this.currentSelectionEle);
            // debugger
            this.scrollingContainerEle = $(CONTAINER_SCROLLING_LIST)[0];
            this.leftNavContainerEle = $(CONTAINER_MAIN)[0];
            this.setSelectedElement(this.currentSelectionEle);
            // this.$el.on("change", this.checkCredentials);
        };

        this.handleControls = function (e) {
            var dirty = false;
            // pressing play triggers select on the media element
            if (e.type === 'buttonpress') {
                switch (e.keyCode) {
                    case buttons.UP:
                    debugger
                    this.incrementCurrentSelectedIndex(-1);
                        // debugger;
                        //  this.setChosenElement();
                        // this.trigger('up');
                        //  debugger;
                        // if (this.buttonView) { //content buttons are visible
                        //     if (this.currentView !== this.buttonView) {
                        //         this.transitionToButtonView();
                        //     } else {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // } else { //no buttons
                        //     if (this.currentView === this.shovelerView) {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // }

                        // if (this.currentView !== this.shovelerView) {
                        //     this.transitionToShovelerView();
                        // } else {
                        //     this.trigger('bounce', null);
                        // }

                        dirty = true;
                        break;
                    case buttons.DOWN:
                    debugger;
                        // this.trigger('deselect');
                        // this.incrementCurrentSelectedIndex(-1);
                        this.trigger('down');
                        //handle button view if we have one
                        // if (this.buttonView) { //content buttons are visible
                        //     if (this.currentView !== this.buttonView) {
                        //         this.transitionToButtonView();
                        //     } else {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // } else { //no buttons
                        //     if (this.currentView === this.shovelerView) {
                        //         this.trigger('bounce', buttons.DOWN);
                        //     }
                        // }

                        dirty = true;
                        break;
                    case buttons.SELECT:
                    debugger;
                        this.trigger('select');
                        // debugger;
                        break;
                }
            }

        }.bind(this);

        this.checkCredentials = function (e) {

            // this.currentSearchQuery = e.target.value;
            this.email = $('#email-input').val();
            this.password = $('#password-input').val();

            $.ajaxSetup({
                async: false
            });
            $.ajax({
                type: "POST",
                url: "https://videos.warwickschiller.com/wp-json/warwickapiauth/v1/login",
                data: {
                    "username": this.email,
                    "password": this.password,
                    "access_token": "",
                    "app_version": "1.4",
                    "country": "USA",
                    "device": "iPhone 8 Plus",
                    "device_os": "13.2",
                    "device_type": "iOS"
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function () {
                    $('#app-loading-spinner').show();
                },
                success: function (result) {
                    if(result.success){
                        this.isLoginUser = 1;
                        this.loggedUserName = result.name;
                        this.loggedUserId = result.user_id;
                        this.trigger('loginSuccess');
                    }else{
                        alert('Wrong Credentials Please Try Again');
                    }
                }.bind(this),

                error: function (response) {
                    console.log(response);
                },
            });

            // if (this.isLoginUser) {
            //     debugger;
            // }

        }.bind(this);

        /**
         * Increment the index of the currently selected item
         * relative to the last selected index.
         * @param {number} direction to move the left nav
         */
        this.incrementCurrentSelectedIndex = function (direction) {

            if ((direction > 0 && this.currSelectedIndex !== (this.$menuItems.length - 1)) || (direction < 0 && this.currSelectedIndex !== 0)) {
                this.currSelectedIndex += direction;
                this.selectLeftNavItem();
            }
        };

        /**
         * Moves the left nav selection in a direction, 1 is down, -1 is up
         */
        this.selectLeftNavItem = function () {
            // update the left nav to the current selection and run the selection animation
            $(this.currentSelectionEle).removeClass(CLASS_MENU_ITEM_SELECTED);
            // debugger;
            this.currentSelectionEle = this.$menuItems.eq(this.currSelectedIndex)[0];
            console.log(this.currentSelectionEle);
            this.setSelectedElement(this.currentSelectionEle);

            this.shiftNavScrollContainer();
            this.select(this.currentSelectionEle);
            //shade the elements farther away from the selection
            // this.trigger('indexChange', this.currSelectedIndex);
        };

        this.shiftNavScrollContainer = function () {
            if (!this.translateAmount) {
                // this.translateAmount = this.currentSelectionEle.getBoundingClientRect().height + 2;
            }

            //shift the nav as selection changes
            var translateHeight = 0 - (this.translateAmount * this.currSelectedIndex);
            this.scrollingContainerEle.style.webkitTransform = "translateY(" + translateHeight + "px)";
        };

        /**
         * Change the style of the selected element to selected
         * @param {Element} ele currently selected element
         */
        this.setSelectedElement = function (ele) {
            ele = ele || this.currentSelectionEle;
            console.log(ele);
            // debugger;

            //remove chosen class if it's there
            if ($(ele).hasClass(CLASS_MENU_ITEM_CHOSEN)) {
                $(ele).removeClass(CLASS_MENU_ITEM_CHOSEN);
            }

            //remove highlighted class if it's there
            var highlightedEle = $("." + CLASS_MENU_ITEM_HIGHLIGHTED);
            if (highlightedEle) {
                highlightedEle.removeClass(CLASS_MENU_ITEM_HIGHLIGHTED);
                this.leftNavContainerEle.classList.remove('leftnav-collapsed-highlight');
            }

            $(ele).addClass(CLASS_MENU_ITEM_SELECTED);
        };

        function makeLogin() {
            this.trigger('select');
        }

        this.select = function (index) {
            //  console.log(index);
            //  debugger
             index.focus();
         }.bind(this);

        this.reset = function () {
            this.currentSearchQuery = null;
            this.$el.val("");
        }.bind(this);

        this.deselect = function () {
            this.$el.blur();
        }.bind(this);
    }

    exports.LogInputView = LogInputView;
}(window));
