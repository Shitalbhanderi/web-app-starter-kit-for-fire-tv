(function(exports) {
    'use strict';

    var settings = {
        Model: YouTubeAPIModel,
        PlayerView: YouTubePlayerView,
        PlaylistView: PlaylistPlayerView,
        showSearch: true,
        user: "amazonwebservices",
        devKey: "AIzaSyD118qYmP_ISb4n8fhjobwAk51dLJYd0Yk",
        createCategoriesFromSections: true
    };

    exports.app = new App(settings);
}(window));
