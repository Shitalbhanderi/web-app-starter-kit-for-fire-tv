
(function(exports) {
    'use strict';
    var settings = {
        Model: JSONMediaModel,
        PlayerView: PlayerView,
        PlaylistView: PlaylistPlayerView,
        // dataURL: "https://videos.warwickschiller.com/wp-json/warwickapi/v1/homedetail?user_id=7921",
        dataURL: "http://warwickvideo.staging.wpengine.com/wp-json/warwickapi/v1/homedetail?user_id=7921",
        // dataURL: "./assets/genericMediaData.json",
        showSearch: true,
        displayButtons:false,
        skipLength: 10,
        controlsHideTime: 3000,
        networkTimeout: 20,
        retryTimes: 3
    };

    exports.app = new App(settings);
}(window));