

(function(exports) {

       var settings = {
        Model: JSONMediaModel,
        PlayerView: PlayerView,
        PlaylistView: PlaylistPlayerView,
        // dataURL: "./assets/genericMediaData.json",
        dataURL: "https://videos.warwickschiller.com/wp-json/warwickapi/v1/homedetail?user_id=7921",

        // dataURL: jsonData,
        // dataURL: "./assets/jsonData.js",
        showSearch: true,
        displayButtons:false,
        skipLength: 10,
        controlsHideTime: 3000,
        networkTimeout: 20,
        retryTimes: 3
    };


    exports.app = new App(settings);

    // console.log(vimoeClient());
    // var Vimeo = require('vimeo').Vimeo;
    // var client = new Vimeo(CLIENT_ID, CLIENT_SECRET, ACCESS_TOKEN);
}(window));


